class LoginPage {
  go(){
    cy.visit('/index.php')
  }

  fillCredentials(username, password){
    cy.get('input#username')
      .as('inputUsername')

    cy.get('input#password')
      .as('inputPassword')

    if (username) {
      cy.get('@inputUsername')
        .type(username)
    }
    if (password) {
      cy.get('@inputPassword')
        .type(password)
    }
  }

  clickOnLoginButton(){
    cy.intercept('GET', '**/zapagentes/index.php**').as('index')
    cy.get('p.clearfix button')
      .should('have.text', 'Login').click()
  }

  checkEnterOnIndexPage(){
    cy.wait('@index')
    cy.title().should('eq', 'Pagina Inicial')
    cy.get('ul[class=app-title]')
      .should('be.visible')
  }

  checkLoginPageAlertsWithoutCredentials(){
    cy.get('div[class=form-error] > p')
      .eq(0)
      .should('have.text', 'Introduza o seu Login')
    cy.get('div[class=form-error] > p')
      .eq(1)
      .should('have.text', 'Introduza a sua password')
  }

  checkLoginPageAlertsWithoutOneCredential(){
    cy.get('@inputUsername')
      .then( ($element) => {
        if($element.hasClass('invalid')){
          cy.get('div[class=form-error] > p')
            .should('have.text', 'Introduza o seu Login')
        }
      })

    cy.get('@inputPassword')
      .then( ($element) => {
        if($element.hasClass('invalid')){
          cy.get('div[class=form-error] > p')
            .should('have.text', 'Introduza a sua password')
        }
      })
    }

  checkAlertDanger(){
    cy.get('div.alert.alert-danger')
      .contains('Não existe nenhum vendedor com as credênciais introduzidas')
  }

}

export default LoginPage