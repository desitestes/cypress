import LoginPage from '../pages/LoginPage'

describe('Login', () => {
  let login = new LoginPage()

  it('with correct credentials', () => {
    login.go()
    login.fillCredentials('username', 'Password123')
    login.clickOnLoginButton()
    login.checkEnterOnIndexPage()
  })

  it('without credentials', () => {
    login.go()
    login.clickOnLoginButton()
    login.checkLoginPageAlertsWithoutCredentials()
  })

  it('without username', () => {
    login.go()
    login.fillCredentials('','Password123')
    login.clickOnLoginButton()
    login.checkLoginPageAlertsWithoutOneCredential()
  })

  it('without password', () => {
    login.go()
    login.fillCredentials('username','')
    login.clickOnLoginButton()
    login.checkLoginPageAlertsWithoutOneCredential()
  })

  it('with wrong password', () => {
    login.go()
    login.fillCredentials('username','ASD159qwe?')
    login.clickOnLoginButton()
    login.checkAlertDanger()
  })

  it('with wrong username', () => {
    login.go()
    login.fillCredentials('username1','ASD159qwe?')
    login.clickOnLoginButton()
    login.checkAlertDanger()
  })

})